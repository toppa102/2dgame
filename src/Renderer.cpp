#include "MappedBuffer.hpp"
#include "StaticBuffer.hpp"
#include <Renderer.hpp>
#include <SDL_error.h>
#include <SDL_events.h>
#include <SDL_video.h>
#include <Utils.hpp>

#include <SDL2/SDL.h>
#include <memory>
#include <spdlog/spdlog.h>
#include <vector>
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_enums.hpp>
#include <vulkan/vulkan_raii.hpp>
#include <vulkan/vulkan_structs.hpp>

const int MAX_FRAMES_IN_FLIGHT = 2;

namespace Renderer
{
    Renderer::Renderer(std::string_view app_name)
        : m_appInfo(vk::ApplicationInfo{
              .sType = vk::StructureType::eApplicationInfo,
              .pApplicationName = app_name.data(),
              .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
              .pEngineName = "Engineless",
              .engineVersion = VK_MAKE_VERSION(0, 0, 1),
              .apiVersion = VK_API_VERSION_1_1}),
          m_window(createWindow()), m_device(createDevice()),
          m_renderPass(createRenderPass()),
          m_descriptorSetLayout(createDescriptorSetLayout()),
          m_pipelineLayout(createPipelineLayout()),
          m_graphicsPipeline(createGraphicsPipeline()),
          m_textureSampler(createTextureSampler()),
          m_texture(m_device.createTexture("asd.png")),
          m_vertexBuffer(m_device.createStaticBuffer<Vertex>(
              vertices, vk::BufferUsageFlagBits::eVertexBuffer)),
          m_indexBuffer(m_device.createStaticBuffer<uint16_t>(
              indices, vk::BufferUsageFlagBits::eIndexBuffer)),
          m_uniformBuffers(createUniformBuffers()),
          m_descriptorPool(createDescriptorPool()),
          m_descriptorSets(createDescriptorSets()),
          m_frameBuffers(createFrameBuffers()),
          m_commandBuffers(createCommandBuffers()),
          m_imageAvailableSemaphores(createSemaphores()),
          m_renderFinishedSemaphores(createSemaphores()),
          m_inFlightFences(createFences())
    {
        bool done = false;
        bool windowMinimized = false;
        while (!done)
        {
            SDL_Event event;

            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                case SDL_WINDOWEVENT:
                {
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                        m_frameBufferResized = true;
                    else if (event.window.event == SDL_WINDOWEVENT_MINIMIZED)
                        windowMinimized = true;
                    else if (event.window.event == SDL_WINDOWEVENT_RESTORED)
                        windowMinimized = false;

                    break;
                }
                case SDL_QUIT:
                    done = true;
                    break;
                default:
                    break;
                }
            }

            if (!windowMinimized)
                drawFrame();
        }
        m_device.get().waitIdle();
    }

    Renderer::~Renderer() {}

    static int resizeWatcher(void* data, SDL_Event* event)
    {
        if (event->type == SDL_WINDOWEVENT &&
            event->window.event == SDL_WINDOWEVENT_RESIZED)
        {
            bool* frameBufferResized = static_cast<bool*>(data);
            *frameBufferResized = true;
        }
        return 0;
    }

    SDLWindowPtr Renderer::createWindow()
    {
        if (SDL_Init(SDL_INIT_VIDEO))
        {
            throw std::runtime_error(SDL_GetError());
        }

        SDL_Window* window = SDL_CreateWindow(
            m_appInfo.pApplicationName, SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, 1280, 720,
            SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

        if (window == nullptr)
        {
            throw std::runtime_error(SDL_GetError());
        }

        SDL_AddEventWatch(resizeWatcher, &m_frameBufferResized);

        return SDLWindowPtr(window, SDL_DestroyWindow);
    }

    Device Renderer::createDevice() const
    {
        return Device(m_window.get(), m_appInfo, m_ctx);
    }

    vk::raii::RenderPass Renderer::createRenderPass()
    {

        vk::AttachmentDescription colorAttachment{
            .format = m_device.getSwapChainDetails().surfaceFormat.format,
            .samples = vk::SampleCountFlagBits::e1,
            .loadOp = vk::AttachmentLoadOp::eClear,
            .storeOp = vk::AttachmentStoreOp::eStore,
            .stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
            .stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
            .initialLayout = vk::ImageLayout::eUndefined,
            .finalLayout = vk::ImageLayout::ePresentSrcKHR};

        vk::AttachmentReference colorAttachmentRef{
            .attachment = 0,
            .layout = vk::ImageLayout::eColorAttachmentOptimal,
        };

        vk::SubpassDescription subpass{
            .pipelineBindPoint = vk::PipelineBindPoint::eGraphics,
            .colorAttachmentCount = 1,
            .pColorAttachments = &colorAttachmentRef};

        vk::SubpassDependency dependency{
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
            .dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
            .srcAccessMask = vk::AccessFlagBits(),
            .dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite};

        vk::RenderPassCreateInfo renderPassInfo{
            .sType = vk::StructureType::eRenderPassCreateInfo,
            .attachmentCount = 1,
            .pAttachments = &colorAttachment,
            .subpassCount = 1,
            .pSubpasses = &subpass,
            .dependencyCount = 1,
            .pDependencies = &dependency};

        return m_device.get().createRenderPass(renderPassInfo);
    }

    vk::raii::DescriptorSetLayout Renderer::createDescriptorSetLayout()
    {
        vk::DescriptorSetLayoutBinding uboLayoutBinding{
            .binding = 0,
            .descriptorType = vk::DescriptorType::eUniformBuffer,
            .descriptorCount = 1,
            .stageFlags = vk::ShaderStageFlagBits::eVertex,
            .pImmutableSamplers = nullptr,
        };

        vk::DescriptorSetLayoutBinding samplerLayoutBinding{
            .binding = 1,
            .descriptorType = vk::DescriptorType::eCombinedImageSampler,
            .descriptorCount = 1,
            .stageFlags = vk::ShaderStageFlagBits::eFragment,
            .pImmutableSamplers = nullptr,
        };

        std::array<vk::DescriptorSetLayoutBinding, 2> bindings = {
            uboLayoutBinding, samplerLayoutBinding};

        vk::DescriptorSetLayoutCreateInfo layoutInfo{
            .sType = vk::StructureType::eDescriptorSetLayoutCreateInfo,
            .bindingCount = 2,
            .pBindings = bindings.data(),
        };

        return m_device.get().createDescriptorSetLayout(layoutInfo);
    }

    vk::raii::PipelineLayout Renderer::createPipelineLayout()
    {
        vk::PipelineLayoutCreateInfo pipelineLayoutInfo{
            .sType = vk::StructureType::ePipelineLayoutCreateInfo,
            .setLayoutCount = 1,
            .pSetLayouts = &*m_descriptorSetLayout,
        };

        return m_device.get().createPipelineLayout(pipelineLayoutInfo);
    }

    vk::raii::ShaderModule
    Renderer::createShaderModule(const std::vector<char>& code)
    {
        vk::ShaderModuleCreateInfo createInfo{
            .sType = vk::StructureType::eShaderModuleCreateInfo,
            .codeSize = code.size(),
            .pCode = reinterpret_cast<const uint32_t*>(code.data())};

        return m_device.get().createShaderModule(createInfo);
    }

    vk::raii::Pipeline Renderer::createGraphicsPipeline()
    {
        auto vertCode = Utils::readFile("shaders/shader.vert.bin");
        auto fragCode = Utils::readFile("shaders/shader.frag.bin");

        auto vertModule = createShaderModule(vertCode);
        auto fragModule = createShaderModule(fragCode);

        vk::PipelineShaderStageCreateInfo vertShaderStageInfo{
            .sType = vk::StructureType::ePipelineShaderStageCreateInfo,
            .stage = vk::ShaderStageFlagBits::eVertex,
            .module = *vertModule,
            .pName = "main"};

        vk::PipelineShaderStageCreateInfo fragShaderStageInfo{
            .sType = vk::StructureType::ePipelineShaderStageCreateInfo,
            .stage = vk::ShaderStageFlagBits::eFragment,
            .module = *fragModule,
            .pName = "main"};

        std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages = {
            vertShaderStageInfo, fragShaderStageInfo};

        auto bindingDescription = Vertex::getBindingDescription();
        auto attributeDescriptions = Vertex::getAttributeDescriptions();

        vk::PipelineVertexInputStateCreateInfo vertexInputInfo{
            .sType = vk::StructureType::ePipelineVertexInputStateCreateInfo,
            .vertexBindingDescriptionCount = 1,
            .pVertexBindingDescriptions = &bindingDescription,
            .vertexAttributeDescriptionCount =
                static_cast<uint32_t>(attributeDescriptions.size()),
            .pVertexAttributeDescriptions = attributeDescriptions.data()};

        vk::PipelineInputAssemblyStateCreateInfo inputAssembly{
            .sType = vk::StructureType::ePipelineInputAssemblyStateCreateInfo,
            .topology = vk::PrimitiveTopology::eTriangleList,
            .primitiveRestartEnable = vk::False,
        };

        vk::PipelineViewportStateCreateInfo viewportState{
            .sType = vk::StructureType::ePipelineViewportStateCreateInfo,
            .viewportCount = 1,
            .scissorCount = 1};

        vk::PipelineRasterizationStateCreateInfo rasterizer{
            .sType = vk::StructureType::ePipelineRasterizationStateCreateInfo,
            .depthClampEnable = vk::False,
            .rasterizerDiscardEnable = VK_FALSE,
            .polygonMode = vk::PolygonMode::eFill,
            .cullMode = vk::CullModeFlagBits::eBack,
            .frontFace = vk::FrontFace::eCounterClockwise,
            .depthBiasEnable = vk::False,
            .lineWidth = 1.0f,
        };

        vk::PipelineMultisampleStateCreateInfo multisampling{
            .sType = vk::StructureType::ePipelineMultisampleStateCreateInfo,
            .rasterizationSamples = vk::SampleCountFlagBits::e1,
            .sampleShadingEnable = vk::False,
        };

        vk::PipelineColorBlendAttachmentState colorBlendAttachment{
            .blendEnable = vk::False,
            .colorWriteMask = vk::ColorComponentFlagBits::eR |
                              vk::ColorComponentFlagBits::eG |
                              vk::ColorComponentFlagBits::eB |
                              vk::ColorComponentFlagBits::eA,
        };

        vk::PipelineColorBlendStateCreateInfo colorBlending{
            .sType = vk::StructureType::ePipelineColorBlendStateCreateInfo,
            .logicOpEnable = vk::False,
            .logicOp = vk::LogicOp::eCopy,
            .attachmentCount = 1,
            .pAttachments = &colorBlendAttachment,
            .blendConstants = {{0.0f, 0.0f, 0.0f, 0.0f}},
        };

        std::vector<vk::DynamicState> dynamicStates = {
            vk::DynamicState::eViewport, vk::DynamicState::eScissor};

        vk::PipelineDynamicStateCreateInfo dynamicState{
            .sType = vk::StructureType::ePipelineDynamicStateCreateInfo,
            .dynamicStateCount = static_cast<uint32_t>(dynamicStates.size()),
            .pDynamicStates = dynamicStates.data()};

        vk::GraphicsPipelineCreateInfo pipelineInfo{
            .sType = vk::StructureType::eGraphicsPipelineCreateInfo,
            .stageCount = 2,
            .pStages = shaderStages.data(),
            .pVertexInputState = &vertexInputInfo,
            .pInputAssemblyState = &inputAssembly,
            .pViewportState = &viewportState,
            .pRasterizationState = &rasterizer,
            .pMultisampleState = &multisampling,
            .pColorBlendState = &colorBlending,
            .pDynamicState = &dynamicState,
            .layout = *m_pipelineLayout,
            .renderPass = *m_renderPass,
            .subpass = 0,
            .basePipelineHandle = VK_NULL_HANDLE,
        };

        return m_device.get().createGraphicsPipeline(nullptr, pipelineInfo);
    }

    vk::raii::Sampler Renderer::createTextureSampler()
    {
        vk::SamplerCreateInfo samplerInfo{
            .sType = vk::StructureType::eSamplerCreateInfo,
            .magFilter = vk::Filter::eLinear,
            .minFilter = vk::Filter::eLinear,
            .mipmapMode = vk::SamplerMipmapMode::eLinear,
            .addressModeU = vk::SamplerAddressMode::eRepeat,
            .addressModeV = vk::SamplerAddressMode::eRepeat,
            .addressModeW = vk::SamplerAddressMode::eRepeat,
            .mipLodBias = 0.0f,
            .anisotropyEnable = vk::True,
            .maxAnisotropy = 16,
            .compareEnable = vk::False,
            .compareOp = vk::CompareOp::eAlways,
            .minLod = 0.0f,
            .maxLod = 0.0f,
            .borderColor = vk::BorderColor::eIntOpaqueBlack,
            .unnormalizedCoordinates = vk::False,
        };

        return m_device.get().createSampler(samplerInfo);
    }

    std::vector<MappedBuffer> Renderer::createUniformBuffers()
    {
        std::vector<MappedBuffer> buffers;

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
        {
            buffers.push_back(m_device.createMappedBuffer(
                sizeof(UniformBufferObject),
                vk::BufferUsageFlagBits::eUniformBuffer));
        }

        return buffers;
    }

    vk::raii::DescriptorPool Renderer::createDescriptorPool()
    {
        std::array<vk::DescriptorPoolSize, 2> poolSizes{
            vk::DescriptorPoolSize{
                .type = vk::DescriptorType::eUniformBuffer,
                .descriptorCount = static_cast<uint32_t>(MAX_FRAMES_IN_FLIGHT)},
            vk::DescriptorPoolSize{
                .type = vk::DescriptorType::eCombinedImageSampler,
                .descriptorCount =
                    static_cast<uint32_t>(MAX_FRAMES_IN_FLIGHT)}};

        vk::DescriptorPoolCreateInfo poolInfo{
            .sType = vk::StructureType::eDescriptorPoolCreateInfo,
            .flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
            .maxSets = static_cast<uint32_t>(MAX_FRAMES_IN_FLIGHT),
            .poolSizeCount = 2,
            .pPoolSizes = poolSizes.data(),
        };

        return m_device.get().createDescriptorPool(poolInfo);
    }

    std::vector<vk::raii::DescriptorSet> Renderer::createDescriptorSets()
    {
        std::vector<vk::DescriptorSetLayout> layouts(MAX_FRAMES_IN_FLIGHT,
                                                     *m_descriptorSetLayout);
        vk::DescriptorSetAllocateInfo allocInfo{
            .sType = vk::StructureType::eDescriptorSetAllocateInfo,
            .descriptorPool = *m_descriptorPool,
            .descriptorSetCount = static_cast<uint32_t>(MAX_FRAMES_IN_FLIGHT),
            .pSetLayouts = layouts.data()};

        auto descriptorSets = m_device.get().allocateDescriptorSets(allocInfo);

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
        {
            vk::DescriptorBufferInfo bufferInfo{
                .buffer = m_uniformBuffers[i].m_buffer,
                .offset = 0,
                .range = sizeof(UniformBufferObject),
            };
            vk::DescriptorImageInfo imageInfo{
                .sampler = *m_textureSampler,
                .imageView = m_texture.m_imageView,
                .imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
            };

            std::array<vk::WriteDescriptorSet, 2> descriptorWrites{};

            descriptorWrites[0] = vk::WriteDescriptorSet{
                .sType = vk::StructureType::eWriteDescriptorSet,
                .dstSet = *m_descriptorSets[i],
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = vk::DescriptorType::eUniformBuffer,
                .pBufferInfo = &bufferInfo};

            descriptorWrites[1] = vk::WriteDescriptorSet{
                .sType = vk::StructureType::eWriteDescriptorSet,
                .dstSet = *m_descriptorSets[i],
                .dstBinding = 1,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = vk::DescriptorType::eCombinedImageSampler,
                .pImageInfo = &imageInfo};

            m_device.get().updateDescriptorSets(descriptorWrites, nullptr);
        }

        return descriptorSets;
    }

    std::vector<vk::raii::Framebuffer> Renderer::createFrameBuffers()
    {
        std::vector<vk::raii::Framebuffer> frameBuffers;

        for (const auto& imageView : m_device.getImageViews())
        {
            vk::FramebufferCreateInfo framebufferInfo{
                .sType = vk::StructureType::eFramebufferCreateInfo,
                .renderPass = *m_renderPass,
                .attachmentCount = 1,
                .pAttachments = &*imageView,
                .width = m_device.getSwapChainDetails().extent.width,
                .height = m_device.getSwapChainDetails().extent.height,
                .layers = 1};

            frameBuffers.push_back(
                m_device.get().createFramebuffer(framebufferInfo));
        }

        return frameBuffers;
    }

    std::vector<vk::raii::CommandBuffer> Renderer::createCommandBuffers()
    {
        vk::CommandBufferAllocateInfo allocInfo{
            .sType = vk::StructureType::eCommandBufferAllocateInfo,
            .commandPool = *m_device.getCommandPool(),
            .level = vk::CommandBufferLevel::ePrimary,
            .commandBufferCount = MAX_FRAMES_IN_FLIGHT};

        return m_device.get().allocateCommandBuffers(allocInfo);
    }

    std::vector<vk::raii::Semaphore> Renderer::createSemaphores()
    {
        std::vector<vk::raii::Semaphore> semaphores;

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
        {
            vk::SemaphoreCreateInfo createInfo{
                .sType = vk::StructureType::eSemaphoreCreateInfo};

            semaphores.push_back(m_device.get().createSemaphore(createInfo));
        }

        return semaphores;
    }

    std::vector<vk::raii::Fence> Renderer::createFences()
    {
        std::vector<vk::raii::Fence> fences;

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
        {
            vk::FenceCreateInfo createInfo{
                .sType = vk::StructureType::eFenceCreateInfo,
                .flags = vk::FenceCreateFlagBits::eSignaled};

            fences.push_back(m_device.get().createFence(createInfo));
        }

        return fences;
    }

    void
    Renderer::recordCommandBuffer(const vk::raii::CommandBuffer& commandBuffer,
                                  uint32_t imageIndex)
    {
        vk::CommandBufferBeginInfo beginInfo{
            .sType = vk::StructureType::eCommandBufferBeginInfo};

        commandBuffer.begin(beginInfo);

        vk::ClearValue clearColor = {.color = {{{0.0f, 0.0f, 0.0f, 1.0f}}}};

        vk::RenderPassBeginInfo renderPassInfo{
            .sType = vk::StructureType::eRenderPassBeginInfo,
            .renderPass = *m_renderPass,
            .framebuffer = *m_frameBuffers[imageIndex],
            .renderArea =
                vk::Rect2D{.extent = m_device.getSwapChainDetails().extent},
            .clearValueCount = 1,
            .pClearValues = &clearColor};

        commandBuffer.beginRenderPass(renderPassInfo,
                                      vk::SubpassContents::eInline);
        commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics,
                                   *m_graphicsPipeline);

        vk::Viewport viewport{
            .x = 0.0f,
            .y = 0.0f,
            .width = (float)m_device.getSwapChainDetails().extent.width,
            .height = (float)m_device.getSwapChainDetails().extent.height,
            .minDepth = 0.0f,
            .maxDepth = 1.0f,
        };

        commandBuffer.setViewport(0, viewport);

        vk::Rect2D scissor{.offset = {0, 0},
                           .extent = m_device.getSwapChainDetails().extent};

        commandBuffer.setScissor(0, scissor);

        vk::DeviceSize offsets = {0};
        commandBuffer.bindVertexBuffers(0, m_vertexBuffer.m_buffer, offsets);
        commandBuffer.bindIndexBuffer(m_indexBuffer.m_buffer, 0,
                                      vk::IndexType::eUint16);
        commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics, *m_pipelineLayout, 0,
            *m_descriptorSets[m_currentFrame], nullptr);

        commandBuffer.drawIndexed(indices.size(), 1, 0, 0, 0);

        commandBuffer.endRenderPass();

        commandBuffer.end();
    }

    void Renderer::recreateSwapChain()
    {
        m_device.get().waitIdle();
        m_frameBuffers.clear();

        m_device.recreateSwapChain();
        m_frameBuffers = createFrameBuffers();
    }

    void Renderer::drawFrame()
    {
        m_device.get().waitForFences(*m_inFlightFences[m_currentFrame],
                                     vk::True, UINT64_MAX);
        vk::Result result = vk::Result::eSuccess;
        uint32_t imageIndex;

        try
        {
            std::tie(result, imageIndex) =
                m_device.getSwapChain().acquireNextImage(
                    UINT64_MAX, *m_imageAvailableSemaphores[m_currentFrame]);
        }
        catch (const vk::OutOfDateKHRError& e)
        {
            recreateSwapChain();
            return;
        }

        if (result != vk::Result::eSuccess &&
            result != vk::Result::eSuboptimalKHR)
        {
            throw std::runtime_error("failed to acquire swap chain image!");
        }

        updateUniformBuffer(m_currentFrame);

        m_device.get().resetFences(*m_inFlightFences[m_currentFrame]);

        m_commandBuffers[m_currentFrame].reset();
        recordCommandBuffer(m_commandBuffers[m_currentFrame], imageIndex);

        vk::PipelineStageFlags waitStages[] = {
            vk::PipelineStageFlagBits::eColorAttachmentOutput};

        vk::Semaphore waitSemaphores[] = {
            *m_imageAvailableSemaphores[m_currentFrame]};
        vk::Semaphore signalSemaphores[] = {
            *m_renderFinishedSemaphores[m_currentFrame]};

        vk::SubmitInfo submitInfo{
            .sType = vk::StructureType::eSubmitInfo,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = waitSemaphores,
            .pWaitDstStageMask = waitStages,
            .commandBufferCount = 1,
            .pCommandBuffers = &*m_commandBuffers[m_currentFrame],
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = signalSemaphores,
        };

        m_device.getGraphicsQueue().submit(submitInfo,
                                           *m_inFlightFences[m_currentFrame]);

        vk::PresentInfoKHR presentInfo{
            .sType = vk::StructureType::ePresentInfoKHR,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = signalSemaphores,
            .swapchainCount = 1,
            .pSwapchains = &*m_device.getSwapChain(),
            .pImageIndices = &imageIndex,
        };

        vk::Result presentResult = vk::Result::eSuccess;
        try
        {
            presentResult = m_device.getPresentQueue().presentKHR(presentInfo);
        }
        catch (const vk::OutOfDateKHRError& e)
        {
            m_frameBufferResized = false;
            recreateSwapChain();
        }

        if (presentResult == vk::Result::eSuboptimalKHR || m_frameBufferResized)
        {
            m_frameBufferResized = false;
            recreateSwapChain();
        }
        else if (presentResult != vk::Result::eSuccess)
        {
            throw std::runtime_error("failed to present swap chain image!");
        }

        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

} // namespace Renderer
