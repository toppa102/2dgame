#include "Image.hpp"
#include "MappedBuffer.hpp"
#include <Device.hpp>
#include <SDL2/SDL_vulkan.h>
#include <SDL_events.h>
#include <cstdint>
#include <iostream>
#include <map>
#include <set>
#include <spdlog/spdlog.h>
#include <vector>

#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_raii.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"};

using namespace Renderer::Utils;

namespace Renderer
{
    const std::vector<const char*> defaultDeviceExtensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME};

    Device::Device(SDL_Window* window, const vk::ApplicationInfo& appInfo,
                   const vk::raii::Context& ctx)
        : m_window(window), m_debugInfo(defaultDebugMessengerCreateInfo()),
          m_instance(createInstance(ctx, appInfo)), m_surface(createSurface()),
          m_physicalDevice(pickPhysicalDevice()),
          m_indices(findQueueFamilies(m_physicalDevice, m_surface)),
          m_device(createLogicalDevice()), m_commandPool(createCommandPool()),
          m_graphicsQueue(
              m_device.getQueue(m_indices.graphicsFamily.value(), 0)),
          m_presentQueue(m_device.getQueue(m_indices.presentFamily.value(), 0)),
          m_swapChainDetails(querySwapChainDetails()),
          m_swapChain(createSwapChain()),
          m_swapChainImages(getSwapChainImages()),
          m_swapChainImageViews(getSwapChainImageViews())
    {
        VmaAllocatorCreateInfo allocatorInfo{.physicalDevice =
                                                 *m_physicalDevice,
                                             .device = *m_device,
                                             .instance = *m_instance};
        vmaCreateAllocator(&allocatorInfo, &m_allocator);
    }

    Device::~Device() { vmaDestroyAllocator(m_allocator); }

    void Device::recreateSwapChain()
    {
        m_device.waitIdle();
        m_swapChainDetails = querySwapChainDetails();

        m_swapChain = createSwapChain(*m_swapChain);
        m_swapChainImages = getSwapChainImages();
        m_swapChainImageViews = getSwapChainImageViews();
    }

    vk::raii::Instance
    Device::createInstance(const vk::raii::Context& ctx,
                           vk::ApplicationInfo applicationInfo)
    {
        if (enableValidationLayers &&
            !checkValidationLayerSupport(validationLayers))
        {
            throw std::runtime_error("Validation layers not available!");
        }

        auto extensions =
            getRequiredInstanceExtensions(m_window, enableValidationLayers);

        vk::InstanceCreateInfo createInfo{
            .sType = vk::StructureType::eInstanceCreateInfo,
            .pNext = &m_debugInfo,
            .pApplicationInfo = &applicationInfo,
            .enabledExtensionCount = static_cast<uint32_t>(extensions.size()),
            .ppEnabledExtensionNames = extensions.data(),
        };

        if (enableValidationLayers)
        {
            createInfo.enabledLayerCount =
                static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
        }
        return ctx.createInstance(createInfo);
    }

    vk::raii::SurfaceKHR Device::createSurface()
    {
        VkSurfaceKHR temp_surface;
        if (SDL_Vulkan_CreateSurface(m_window,
                                     static_cast<VkInstance>(*m_instance),
                                     &temp_surface) != SDL_TRUE)
        {
            throw std::runtime_error("Failed to create window surface!");
        }

        return vk::raii::SurfaceKHR(m_instance, temp_surface);
    }

    vk::raii::PhysicalDevice Device::pickPhysicalDevice()
    {
        std::multimap<size_t, vk::raii::PhysicalDevice> candidates;


        for (auto& device : m_instance.enumeratePhysicalDevices())
        {
            size_t score = 0;

            if (device.getProperties().deviceType ==
                vk::PhysicalDeviceType::eDiscreteGpu)
            {
                score += 1000;
            }

            score += device.getProperties().limits.maxImageDimension2D;

            if (!isDeviceSuitable(device, m_surface, defaultDeviceExtensions))
            {
                continue;
            }

            candidates.insert(std::make_pair(score, std::move(device)));
        }

        if (candidates.empty())
        {
            throw std::runtime_error(
                "Failed to find a suitable Vulkan device!");
        }

        spdlog::info("{}", candidates.rbegin()->second.getProperties().deviceName.data());

        return std::move(candidates.rbegin()->second);
    }

    vk::raii::Device Device::createLogicalDevice()
    {
        m_indices = findQueueFamilies(m_physicalDevice, m_surface);

        std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
        std::set<uint32_t> uniqueQueueFamilies = {
            m_indices.graphicsFamily.value(), m_indices.presentFamily.value()};

        float queuePriority = 1.0f;

        for (uint32_t queueFamily : uniqueQueueFamilies)
        {
            queueCreateInfos.push_back(vk::DeviceQueueCreateInfo{
                .sType = vk::StructureType::eDeviceQueueCreateInfo,
                .queueFamilyIndex = queueFamily,
                .queueCount = 1,
                .pQueuePriorities = &queuePriority});
        }

        vk::PhysicalDeviceFeatures deviceFeatures{.samplerAnisotropy =
                                                      vk::True};

        vk::DeviceCreateInfo createInfo{
            .sType = vk::StructureType::eDeviceCreateInfo,
            .queueCreateInfoCount =
                static_cast<uint32_t>(queueCreateInfos.size()),
            .pQueueCreateInfos = queueCreateInfos.data(),
            .enabledLayerCount = static_cast<uint32_t>(validationLayers.size()),
            .ppEnabledLayerNames = validationLayers.data(),
            .enabledExtensionCount =
                static_cast<uint32_t>(defaultDeviceExtensions.size()),
            .ppEnabledExtensionNames = defaultDeviceExtensions.data(),
            .pEnabledFeatures = &deviceFeatures,
        };

        m_indices = findQueueFamilies(m_physicalDevice, m_surface);

        return m_physicalDevice.createDevice(createInfo);
    }

    vk::raii::CommandPool Device::createCommandPool()
    {
        vk::CommandPoolCreateInfo poolInfo{
            .sType = vk::StructureType::eCommandPoolCreateInfo,
            .flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
            .queueFamilyIndex = getQueueFamilyIndices().graphicsFamily.value()};

        return m_device.createCommandPool(poolInfo);
    }

    SwapChainDetails Device::querySwapChainDetails() const
    {
        SwapChainSupportDetails swapChainSupport =
            querySwapChainSupport(m_physicalDevice, m_surface);

        vk::SurfaceFormatKHR surfaceFormat =
            chooseSwapSurfaceFormat(swapChainSupport.formats);
        vk::PresentModeKHR presentMode =
            chooseSwapPresentMode(swapChainSupport.presentModes);
        vk::Extent2D extent =
            chooseSwapExtent(swapChainSupport.capabilities, m_window);

        uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
        if (swapChainSupport.capabilities.maxImageCount > 0 &&
            imageCount > swapChainSupport.capabilities.maxImageCount)
        {
            imageCount = swapChainSupport.capabilities.maxImageCount;
        }

        return SwapChainDetails{.surfaceFormat = surfaceFormat,
                                .presentMode = presentMode,
                                .extent = extent,
                                .imageCount = imageCount};
    }

    vk::raii::SwapchainKHR Device::createSwapChain(vk::SwapchainKHR old)
    {
        SwapChainSupportDetails swapChainSupport =
            querySwapChainSupport(m_physicalDevice, m_surface);
        m_swapChainDetails = querySwapChainDetails();

        QueueFamilyIndices indices =
            findQueueFamilies(m_physicalDevice, m_surface);
        std::array<uint32_t, 2> queueFamilyIndices = {
            indices.graphicsFamily.value(), indices.presentFamily.value()};

        bool queuesDiffer = indices.graphicsFamily != indices.presentFamily;

        vk::SwapchainCreateInfoKHR createInfo{
            .sType = vk::StructureType::eSwapchainCreateInfoKHR,
            .surface = *m_surface,
            .minImageCount = m_swapChainDetails.imageCount,
            .imageFormat = m_swapChainDetails.surfaceFormat.format,
            .imageColorSpace = m_swapChainDetails.surfaceFormat.colorSpace,
            .imageExtent = m_swapChainDetails.extent,
            .imageArrayLayers = 1,
            .imageUsage = vk::ImageUsageFlagBits::eColorAttachment,

            .imageSharingMode = queuesDiffer ? vk::SharingMode::eConcurrent
                                             : vk::SharingMode::eExclusive,
            .queueFamilyIndexCount =
                queuesDiffer ? static_cast<uint32_t>(queueFamilyIndices.size())
                             : 0,
            .pQueueFamilyIndices =
                queuesDiffer ? queueFamilyIndices.data() : nullptr,

            .preTransform = swapChainSupport.capabilities.currentTransform,
            .compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque,
            .presentMode = m_swapChainDetails.presentMode,
            .clipped = vk::True,
            .oldSwapchain = old};

        return vk::raii::SwapchainKHR(m_device, createInfo);
    }
    std::vector<vk::Image> Device::getSwapChainImages()
    {
        return m_swapChain.getImages();
    }

    std::vector<vk::raii::ImageView> Device::getSwapChainImageViews()
    {
        std::vector<vk::raii::ImageView> imageViews;

        for (vk::Image image : m_swapChainImages)
        {
            vk::ImageViewCreateInfo createInfo{
                .sType = vk::StructureType::eImageViewCreateInfo,
                .image = image,
                .viewType = vk::ImageViewType::e2D,
                .format = m_swapChainDetails.surfaceFormat.format,
                .components = {.r = vk::ComponentSwizzle::eIdentity,
                               .g = vk::ComponentSwizzle::eIdentity,
                               .b = vk::ComponentSwizzle::eIdentity,
                               .a = vk::ComponentSwizzle::eIdentity},
                .subresourceRange = {.aspectMask =
                                         vk::ImageAspectFlagBits::eColor,
                                     .baseMipLevel = 0,
                                     .levelCount = 1,
                                     .baseArrayLayer = 0,
                                     .layerCount = 1}};
            imageViews.push_back(m_device.createImageView(createInfo));
        }

        return imageViews;
    }

    vk::raii::CommandBuffer Device::beginSingleTimeCmds()
    {
        vk::CommandBufferAllocateInfo allocInfo{
            .sType = vk::StructureType::eCommandBufferAllocateInfo,
            .commandPool = *m_commandPool,
            .level = vk::CommandBufferLevel::ePrimary,
            .commandBufferCount = 1,
        };

        vk::raii::CommandBuffer commandBuffer =
            std::move(m_device.allocateCommandBuffers(allocInfo)[0]);

        vk::CommandBufferBeginInfo beginInfo{
            .sType = vk::StructureType::eCommandBufferBeginInfo,
            .flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit,
        };

        commandBuffer.begin(beginInfo);

        return commandBuffer;
    }

    void
    Device::endSingleTimeCmds(const vk::raii::CommandBuffer&& commandBuffer)
    {
        commandBuffer.end();

        vk::SubmitInfo submitInfo{
            .sType = vk::StructureType::eSubmitInfo,
            .commandBufferCount = 1,
            .pCommandBuffers = &*commandBuffer,
        };
        vk::FenceCreateInfo fenceInfo{
            .sType = vk::StructureType::eFenceCreateInfo,
        };
        vk::raii::Fence waitFence = m_device.createFence(fenceInfo);
        m_device.resetFences(*waitFence);

        m_graphicsQueue.submit(submitInfo, *waitFence);

        if (m_device.waitForFences(*waitFence, vk::True, UINT64_MAX) !=
            vk::Result::eSuccess)
        {
            throw std::runtime_error("waitForFences failed...");
        }
    }

    MappedBuffer Device::createMappedBuffer(vk::DeviceSize size,
                                            vk::BufferUsageFlags bufferUsage)
    {
        return MappedBuffer(m_allocator, size, bufferUsage);
    }

    Image Device::createTexture(std::string_view path)
    {

        int texWidth, texHeight, texChannels;
        stbi_uc* pixels = stbi_load(path.data(), &texWidth, &texHeight,
                                    &texChannels, STBI_rgb_alpha);
        vk::DeviceSize imageSize = texWidth * texHeight * 4;

        if (pixels == nullptr)
        {
            throw std::runtime_error("failed to load texture image!");
        }

        auto image = Image(m_allocator, texWidth, texHeight,
                           vk::Format::eR8G8B8A8Srgb, vk::ImageTiling::eOptimal,
                           vk::ImageUsageFlagBits::eTransferDst |
                               vk::ImageUsageFlagBits::eSampled);
        MappedBuffer stagingBuffer(m_allocator, imageSize,
                                   vk::BufferUsageFlagBits::eTransferSrc);
        memcpy(stagingBuffer.m_data, pixels, imageSize);
        stbi_image_free(pixels);

        transitionImageLayout(image, vk::Format::eR8G8B8A8Srgb,
                              vk::ImageLayout::eUndefined,
                              vk::ImageLayout::eTransferDstOptimal);

        auto commandBuffer = beginSingleTimeCmds();

        vk::BufferImageCopy region{
            .bufferOffset = 0,
            .bufferRowLength = 0,
            .bufferImageHeight = 0,

            .imageSubresource =
                {
                    .aspectMask = vk::ImageAspectFlagBits::eColor,
                    .mipLevel = 0,
                    .baseArrayLayer = 0,
                    .layerCount = 1,
                },

            .imageOffset = {0, 0, 0},
            .imageExtent = {static_cast<uint32_t>(texWidth),
                            static_cast<uint32_t>(texHeight), 1},
        };

        commandBuffer.copyBufferToImage(stagingBuffer.m_buffer, image.m_image,
                                        vk::ImageLayout::eTransferDstOptimal,
                                        region);

        endSingleTimeCmds(std::move(commandBuffer));

        transitionImageLayout(image, vk::Format::eR8G8B8A8Srgb,
                              vk::ImageLayout::eTransferDstOptimal,
                              vk::ImageLayout::eShaderReadOnlyOptimal);

        return image;
    }

    void Device::transitionImageLayout(const Image& image, vk::Format format,
                                       vk::ImageLayout oldLayout,
                                       vk::ImageLayout newLayout)
    {
        auto commandBuffer = beginSingleTimeCmds();

        vk::ImageMemoryBarrier barrier{
            .sType = vk::StructureType::eImageMemoryBarrier,
            .oldLayout = oldLayout,
            .newLayout = newLayout,

            .srcQueueFamilyIndex = vk::QueueFamilyIgnored,
            .dstQueueFamilyIndex = vk::QueueFamilyIgnored,

            .image = image.m_image,
            .subresourceRange{.aspectMask = vk::ImageAspectFlagBits::eColor,
                              .baseMipLevel = 0,
                              .levelCount = 1,
                              .baseArrayLayer = 0,
                              .layerCount = 1}};

        vk::PipelineStageFlags sourceStage;
        vk::PipelineStageFlags destinationStage;

        if (oldLayout == vk::ImageLayout::eUndefined &&
            newLayout == vk::ImageLayout::eTransferDstOptimal)
        {
            barrier.srcAccessMask = {};
            barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

            sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
            destinationStage = vk::PipelineStageFlagBits::eTransfer;
        }
        else if (oldLayout == vk::ImageLayout::eTransferDstOptimal &&
                 newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
        {
            barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
            barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

            sourceStage = vk::PipelineStageFlagBits::eTransfer;
            destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
        }
        else
        {
            throw std::invalid_argument("unsupported layout transition!");
        }

        commandBuffer.pipelineBarrier(sourceStage, destinationStage, {}, 0, 0,
                                      barrier);

        endSingleTimeCmds(std::move(commandBuffer));
    }

} // namespace Renderer
