#include "spdlog/spdlog.h"
#include <MappedBuffer.hpp>
#include <stdexcept>

namespace Renderer
{
    MappedBuffer::MappedBuffer(VmaAllocator allocator, vk::DeviceSize size,
                               vk::BufferUsageFlags vkUsage)
    {
        VkBufferCreateInfo bufferInfo = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = size,
            .usage = static_cast<VkBufferUsageFlags>(vkUsage),
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE};

        VmaAllocationCreateInfo allocInfo = {
            .flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
            .usage = VMA_MEMORY_USAGE_AUTO};

        VkBuffer buffer;
        VmaAllocation allocation;
        if (vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer,
                            &allocation, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("vmaCreateBuffer failed in Buffer.cpp...");
        }

        void* pData;
        if (vmaMapMemory(allocator, allocation, &pData) != VK_SUCCESS)
        {
            throw std::runtime_error("vmaMapMemory failed in Buffer.cpp...");
        }

        this->m_data = pData;
        this->m_allocator = allocator;
        this->m_buffer = buffer;
        this->m_allocation = allocation;
    }

    MappedBuffer::~MappedBuffer()
    {
        if (m_allocator != nullptr && m_allocation != nullptr)
        {
            vmaUnmapMemory(m_allocator, m_allocation);
            vmaDestroyBuffer(m_allocator, m_buffer, m_allocation);
        }
    }
}; // namespace Renderer
