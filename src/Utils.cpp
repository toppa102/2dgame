#include <Utils.hpp>

#include <SDL2/SDL_vulkan.h>
#include <fstream>
#include <ios>
#include <iostream>
#include <optional>
#include <set>
#include <vector>

namespace Renderer::Utils
{
    vk::DebugUtilsMessengerCreateInfoEXT defaultDebugMessengerCreateInfo()
    {
        return vk::DebugUtilsMessengerCreateInfoEXT{

            .sType = vk::StructureType::eDebugUtilsMessengerCreateInfoEXT,
            .messageSeverity =
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
                vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
            .messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
                           vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
                           vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
            .pfnUserCallback = debugCallback};
    }

    bool checkValidationLayerSupport(
        const std::vector<const char*>& validationLayers)
    {
        auto availableLayers = vk::enumerateInstanceLayerProperties();

        for (const char* layerName : validationLayers)
        {
            bool layerFound = false;

            for (const auto& layerProperties : availableLayers)
            {
                if (strcmp(layerName, layerProperties.layerName) == 0)
                {
                    layerFound = true;
                    break;
                }
            }

            if (!layerFound)
            {
                return false;
            }
        }

        return true;
    }

    std::vector<const char*>
    getRequiredInstanceExtensions(SDL_Window* window,
                                  const bool enableValidationLayers)
    {
        uint32_t extensionCount = 0;

        SDL_Vulkan_GetInstanceExtensions(
            window, reinterpret_cast<uint32_t*>(&extensionCount), NULL);
        std::vector<const char*> extensions(extensionCount);
        SDL_Vulkan_GetInstanceExtensions(window, &extensionCount,
                                         extensions.data());

        if (enableValidationLayers)
        {
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        return extensions;
    }

    QueueFamilyIndices
    findQueueFamilies(const vk::raii::PhysicalDevice& physicalDevice,
                      const vk::raii::SurfaceKHR& surface)
    {
        QueueFamilyIndices indices;

        auto queueFamilies = physicalDevice.getQueueFamilyProperties();

        uint32_t i = 0;
        for (const auto& queueFamily : queueFamilies)
        {
            if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics)
                indices.graphicsFamily = i;

            if (physicalDevice.getSurfaceSupportKHR(i, *surface))
                indices.presentFamily = i;

            if (indices)
                break;

            ++i;
        }

        return indices;
    }

    SwapChainSupportDetails
    querySwapChainSupport(const vk::raii::PhysicalDevice& device,
                          const vk::raii::SurfaceKHR& surface)
    {
        return SwapChainSupportDetails{
            .capabilities = device.getSurfaceCapabilitiesKHR(*surface),
            .formats = device.getSurfaceFormatsKHR(*surface),
            .presentModes = device.getSurfacePresentModesKHR(*surface),
        };
    }

    bool checkDeviceExtensionSupport(
        const vk::raii::PhysicalDevice& device,
        const std::vector<const char*>& deviceExtensions)
    {
        auto availableExtensions = device.enumerateDeviceExtensionProperties();

        std::set<std::string> requiredExtensions(deviceExtensions.begin(),
                                                 deviceExtensions.end());

        for (const auto& extension : availableExtensions)
        {
            requiredExtensions.erase(extension.extensionName.data());
        }

            
        return requiredExtensions.empty();
    }

    bool isDeviceSuitable(const vk::raii::PhysicalDevice& device,
                          const vk::raii::SurfaceKHR& surface,
                          const std::vector<const char*>& deviceExtensions)
    {
        QueueFamilyIndices indices = findQueueFamilies(device, surface);

        bool extensionsSupported =
            checkDeviceExtensionSupport(device, deviceExtensions);

        bool swapchainAdequate = false;

        if (extensionsSupported)
        {
            SwapChainSupportDetails swapChainSupport =
                querySwapChainSupport(device, surface);

            swapchainAdequate = !swapChainSupport.formats.empty() &&
                                !swapChainSupport.presentModes.empty();
        }

        return indices && extensionsSupported && swapchainAdequate;
    }

    vk::SurfaceFormatKHR chooseSwapSurfaceFormat(
        const std::vector<vk::SurfaceFormatKHR>& availableFormats)
    {
        for (const auto& availableFormat : availableFormats)
        {
            if (availableFormat.format == vk::Format::eB8G8R8A8Srgb &&
                availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            {
                return availableFormat;
            }
        }

        return availableFormats[0];
    }

    vk::PresentModeKHR chooseSwapPresentMode(
        const std::vector<vk::PresentModeKHR>& availablePresentModes)
    {
        for (const auto& availablePresentMode : availablePresentModes)
        {
            if (availablePresentMode == vk::PresentModeKHR::eMailbox)
            {
                return availablePresentMode;
            }
        }

        return vk::PresentModeKHR::eFifo;
    }

    vk::Extent2D chooseSwapExtent(const vk::SurfaceCapabilitiesKHR capabilities,
                                  SDL_Window* window)
    {
        if (capabilities.currentExtent.width != UINT32_MAX)
        {
            return capabilities.currentExtent;
        }
        else
        {
            int width, height;
            SDL_Vulkan_GetDrawableSize(window, &width, &height);
            return vk::Extent2D{
                .width = std::clamp(static_cast<uint32_t>(width),
                                    capabilities.minImageExtent.width,
                                    capabilities.maxImageExtent.width),
                .height = std::clamp(static_cast<uint32_t>(height),
                                     capabilities.minImageExtent.height,
                                     capabilities.maxImageExtent.height)};
        }
    }

    std::vector<char> readFile(std::string_view fileName)
    {
        std::ifstream f(fileName.data(), std::ios::binary | std::ios::ate);

        auto size = f.tellg();

        f.seekg(std::ios::beg);
        std::vector<char> data(size);
        f.read(data.data(), size);

        return data;
    }
} // namespace Renderer::Utils