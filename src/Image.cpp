#include <Image.hpp>

#include <stdexcept>
#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan_enums.hpp>

namespace Renderer
{
    Image::Image(VmaAllocator allocator, uint32_t width, uint32_t height,
                 vk::Format format, vk::ImageTiling tiling,
                 vk::ImageUsageFlags usage)
    {
        VkImageCreateInfo imageInfo{
            .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
            .imageType = VK_IMAGE_TYPE_2D,
            .format = static_cast<VkFormat>(format),
            .extent = {.width = width, .height = height, .depth = 1},
            .mipLevels = 1,
            .arrayLayers = 1,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .tiling = static_cast<VkImageTiling>(tiling),
            .usage = static_cast<VkImageUsageFlags>(usage),
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED};
        VmaAllocationCreateInfo allocInfo = {.usage = VMA_MEMORY_USAGE_AUTO};

        VkImage image;
        VmaAllocation allocation;
        vmaCreateImage(allocator, &imageInfo, &allocInfo, &image, &allocation,
                       nullptr);

        VmaAllocatorInfo allocatorInfo;
        vmaGetAllocatorInfo(allocator, &allocatorInfo);

        vk::ImageViewCreateInfo viewInfo{
            .sType = vk::StructureType::eImageViewCreateInfo,
            .image = image,
            .viewType = vk::ImageViewType::e2D,
            .format = format,
            .subresourceRange = {.aspectMask = vk::ImageAspectFlagBits::eColor,
                                 .baseMipLevel = 0,
                                 .levelCount = 1,
                                 .baseArrayLayer = 0,
                                 .layerCount = 1}};

        auto imageView = static_cast<vk::Device>(allocatorInfo.device)
                             .createImageView(viewInfo);

        this->m_allocator = allocator;
        this->m_image = image;
        this->m_imageView = imageView;
        this->m_allocation = allocation;
    }

    Image::~Image()
    {
        if (m_allocator != nullptr && m_image != nullptr &&
            m_allocation != nullptr)
        {
            VmaAllocatorInfo allocatorInfo;
            vmaGetAllocatorInfo(m_allocator, &allocatorInfo);
            static_cast<vk::Device>(allocatorInfo.device)
                .destroyImageView(m_imageView);

            vmaDestroyImage(m_allocator, m_image, m_allocation);
        }
    }
}; // namespace Renderer