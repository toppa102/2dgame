#include <StaticBuffer.hpp>

#include <stdexcept>

namespace Renderer
{
    StaticBuffer::StaticBuffer(VmaAllocator allocator, vk::DeviceSize size,
                               vk::BufferUsageFlags vkUsage)
    {
        VkBufferCreateInfo bufferInfo = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = size,
            .usage = static_cast<VkBufferUsageFlags>(vkUsage),
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE};

        VmaAllocationCreateInfo allocInfo = {.usage = VMA_MEMORY_USAGE_AUTO};

        VkBuffer buffer;
        VmaAllocation allocation;
        if (vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer,
                            &allocation, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error(
                "vmaCreateBuffer failed in StaticBuffer.cpp...");
        }

        this->m_allocator = allocator;
        this->m_buffer = buffer;
        this->m_allocation = allocation;
    }

    StaticBuffer::~StaticBuffer()
    {
        if (m_allocator != nullptr)
            vmaDestroyBuffer(m_allocator, m_buffer, m_allocation);
    }
}; // namespace Renderer