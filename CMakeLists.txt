﻿cmake_minimum_required (VERSION 3.8)

project ("vulkanrenderer")

find_package(Vulkan COMPONENTS glslc REQUIRED)
find_package(SDL2 REQUIRED)
find_package(glm REQUIRED)
find_package(spdlog REQUIRED)
add_subdirectory("third_party/VulkanMemoryAllocator")

find_program(glslc_executable NAMES glslc HINTS Vulkan::glslc)

function(compile_shader target)
    cmake_parse_arguments(PARSE_ARGV 1 arg "" "ENV;FORMAT" "SOURCES")
    foreach(source ${arg_SOURCES})
        add_custom_command(
            OUTPUT ${source}.${arg_FORMAT}
            DEPENDS ${source}
            DEPFILE ${source}.d
            COMMAND
                ${glslc_executable}
                $<$<BOOL:${arg_ENV}>:--target-env=${arg_ENV}>
                $<$<BOOL:${arg_FORMAT}>:-mfmt=${arg_FORMAT}>
                -MD -MF ${source}.d
                -o ${source}.${arg_FORMAT}
                ${CMAKE_CURRENT_SOURCE_DIR}/${source}
        )
        target_sources(${target} PRIVATE ${source}.${arg_FORMAT})
    endforeach()
endfunction()

add_executable (vulkanrenderer
	"src/Renderer.cpp"
	"src/Utils.cpp"
	"src/Main.cpp"
	"src/Device.cpp"
    "src/StaticBuffer.cpp"
    "src/MappedBuffer.cpp"
    "src/Image.cpp")


set_target_properties(vulkanrenderer PROPERTIES CXX_STANDARD 20)

if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    set_source_files_properties(
        "src/Device.cpp"
        PROPERTIES
        COMPILE_FLAGS "-w")
endif()

target_link_libraries(vulkanrenderer
	PRIVATE ${SDL2_LIBRARIES}
	PRIVATE Vulkan::Vulkan
	PRIVATE spdlog::spdlog
    PRIVATE GPUOpen::VulkanMemoryAllocator)

target_include_directories(vulkanrenderer
	PRIVATE "include")

target_include_directories(vulkanrenderer
    PRIVATE "third_party/stb_image")

target_compile_definitions(vulkanrenderer
	PRIVATE VULKAN_HPP_NO_CONSTRUCTORS)
    
target_precompile_headers(vulkanrenderer
	PUBLIC "<vulkan/vulkan.hpp>"
    PUBLIC "<vulkan/vulkan_raii.hpp>"
    PUBLIC "<vulkan/vulkan_handles.hpp>")

target_compile_options(vulkanrenderer PRIVATE -fsanitize=address)
target_link_options(vulkanrenderer PRIVATE -fsanitize=address)

compile_shader(vulkanrenderer
    ENV vulkan
    FORMAT bin
    SOURCES
        shaders/shader.vert
        shaders/shader.frag
)