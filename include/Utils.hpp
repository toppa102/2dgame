#pragma once
#include <SDL2/SDL.h>
#include <optional>
#include <spdlog/spdlog.h>
#include <vector>

#include <vulkan/vulkan_handles.hpp>

namespace Renderer::Utils
{
    struct QueueFamilyIndices
    {
        std::optional<uint32_t> graphicsFamily;
        std::optional<uint32_t> presentFamily;

        explicit operator bool()
        {
            return graphicsFamily.has_value() && presentFamily.has_value();
        }
    };

    struct SwapChainSupportDetails
    {
        vk::SurfaceCapabilitiesKHR capabilities;
        std::vector<vk::SurfaceFormatKHR> formats;
        std::vector<vk::PresentModeKHR> presentModes;
    };

    static VKAPI_ATTR VkBool32 VKAPI_CALL
    debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                  VkDebugUtilsMessageTypeFlagsEXT messageType,
                  const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                  void* pUserData)
    {
        if (vk::DebugUtilsMessageSeverityFlagsEXT(messageSeverity) ==
            vk::DebugUtilsMessageSeverityFlagBitsEXT::eError)
        {
            spdlog::critical(pCallbackData->pMessage);
        }
        else if (vk::DebugUtilsMessageSeverityFlagsEXT(messageSeverity) ==
                 vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning)
        {
            spdlog::warn(pCallbackData->pMessage);
        }
        else
        {
            spdlog::info(pCallbackData->pMessage);
        }

        return VK_FALSE;
    }

    vk::DebugUtilsMessengerCreateInfoEXT defaultDebugMessengerCreateInfo();

    bool checkValidationLayerSupport(
        const std::vector<const char*>& validationLayers);

    std::vector<const char*>
    getRequiredInstanceExtensions(SDL_Window* window,
                                  const bool enableValidationLayers);

    QueueFamilyIndices
    findQueueFamilies(const vk::raii::PhysicalDevice& physicalDevice,
                      const vk::raii::SurfaceKHR& surface);

    SwapChainSupportDetails
    querySwapChainSupport(const vk::raii::PhysicalDevice& device,
                          const vk::raii::SurfaceKHR& surface);

    bool checkDeviceExtensionSupport(
        const vk::raii::PhysicalDevice& device,
        const std::vector<const char*>& deviceExtensions);

    bool isDeviceSuitable(const vk::raii::PhysicalDevice& device,
                          const vk::raii::SurfaceKHR& surface,
                          const std::vector<const char*>& deviceExtensions);

    vk::SurfaceFormatKHR chooseSwapSurfaceFormat(
        const std::vector<vk::SurfaceFormatKHR>& availableFormats);

    vk::PresentModeKHR chooseSwapPresentMode(
        const std::vector<vk::PresentModeKHR>& availablePresentModes);

    vk::Extent2D chooseSwapExtent(const vk::SurfaceCapabilitiesKHR capabilities,
                                  SDL_Window* window);

    std::vector<char> readFile(std::string_view fileName);
} // namespace Renderer::Utils
