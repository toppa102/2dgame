#pragma once
#include <utility>
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_handles.hpp>

namespace Renderer
{
    class StaticBuffer
    {
    public:
        StaticBuffer() = delete;
        StaticBuffer(StaticBuffer&) = delete;
        StaticBuffer(StaticBuffer&& o)
            : m_allocation(std::exchange(o.m_allocation, nullptr)),
              m_buffer(std::exchange(o.m_buffer, nullptr)),
              m_allocator(o.m_allocator)
        {
        }

        StaticBuffer(VmaAllocator allocator, vk::DeviceSize size,
                     vk::BufferUsageFlags vkUsage);
        ~StaticBuffer();

        VmaAllocation m_allocation;
        vk::Buffer m_buffer;

    private:
        VmaAllocator m_allocator;
    };
} // namespace Renderer