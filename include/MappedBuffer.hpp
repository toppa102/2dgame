#pragma once
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_handles.hpp>

namespace Renderer
{
    class MappedBuffer
    {
    public:
        MappedBuffer() = delete;
        MappedBuffer(MappedBuffer&) = delete;
        MappedBuffer(MappedBuffer&& o)
            : m_allocation(std::exchange(o.m_allocation, nullptr)),
              m_buffer(std::exchange(o.m_buffer, nullptr)),
              m_data(std::exchange(o.m_data, nullptr)),
              m_allocator(o.m_allocator)
        {
        }

        MappedBuffer(VmaAllocator allocator, vk::DeviceSize size,
                     vk::BufferUsageFlags vkUsage);
        ~MappedBuffer();

        VmaAllocation m_allocation;
        vk::Buffer m_buffer;
        void* m_data;

    private:
        VmaAllocator m_allocator;
    };
} // namespace Renderer