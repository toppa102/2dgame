#pragma once
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_handles.hpp>

namespace Renderer
{
    class Image
    {
    public:
        Image() = delete;
        Image(Image&) = delete;
        Image(const Image&) = delete;
        Image(Image&& o)
            : m_allocation(std::exchange(o.m_allocation, nullptr)),
              m_image(std::exchange(o.m_image, nullptr)),
              m_allocator(o.m_allocator)
        {
        }

        Image(VmaAllocator allocator, uint32_t width, uint32_t height,
              vk::Format format, vk::ImageTiling tiling,
              vk::ImageUsageFlags usage);
        ~Image();

        VmaAllocation m_allocation;
        vk::Image m_image;
        vk::ImageView m_imageView;

    private:
        VmaAllocator m_allocator;
    };
} // namespace Renderer