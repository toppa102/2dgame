#pragma once
#include "spdlog/spdlog.h"
#include <Image.hpp>
#include <MappedBuffer.hpp>
#include <StaticBuffer.hpp>
#include <Utils.hpp>

#include <SDL2/SDL.h>
#include <stdexcept>
#include <vk_mem_alloc.h>
#include <vulkan/vulkan_handles.hpp>

namespace Renderer
{
    struct SwapChainDetails
    {
        vk::SurfaceFormatKHR surfaceFormat;
        vk::PresentModeKHR presentMode;
        vk::Extent2D extent;
        uint32_t imageCount;
    };

    ///
    /// Renderer::Device is a wrapper for sdl window, vulkan surface,
    /// logical device, swapchain and buffer creation.
    ///
    class Device
    {
    public:
        Device(SDL_Window* window, const vk::ApplicationInfo& appInfo,
               const vk::raii::Context& ctx);
        ~Device();

        const vk::raii::Device& get() const { return m_device; }
        const vk::raii::PhysicalDevice& physicalDevice() const
        {
            return m_physicalDevice;
        }
        const vk::raii::SurfaceKHR& surface() const { return m_surface; }
        const vk::raii::SwapchainKHR& getSwapChain() const
        {
            return m_swapChain;
        }

        const std::vector<vk::raii::ImageView>& getImageViews() const
        {
            return m_swapChainImageViews;
        }

        const SwapChainDetails& getSwapChainDetails() const
        {
            return m_swapChainDetails;
        };
        Utils::QueueFamilyIndices getQueueFamilyIndices() const
        {
            return m_indices;
        }

        const vk::raii::Queue& getGraphicsQueue() const
        {
            return m_graphicsQueue;
        }
        const vk::raii::Queue& getPresentQueue() const
        {
            return m_presentQueue;
        }

        const vk::raii::CommandPool& getCommandPool() const
        {
            return m_commandPool;
        }

        void recreateSwapChain();

        ///
        /// Create a StaticBuffer object that isn't host visible.
        ///

        template <typename T>
        StaticBuffer createStaticBuffer(const vk::ArrayProxy<T>& data,
                                        vk::BufferUsageFlags bufferUsage)
        {
            const size_t size = data.size() * sizeof(T);

            StaticBuffer buffer(m_allocator, size,
                                bufferUsage |
                                    vk::BufferUsageFlagBits::eTransferDst);

            VkMemoryPropertyFlags propertyFlags;
            vmaGetAllocationMemoryProperties(m_allocator, buffer.m_allocation,
                                             &propertyFlags);
            if (propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
            {
                void* pData;
                vmaMapMemory(m_allocator, buffer.m_allocation, &pData);
                memcpy(pData, data.begin(), size);
                vmaUnmapMemory(m_allocator, buffer.m_allocation);

                return buffer;
            }

            MappedBuffer stagingBuffer(m_allocator, size,
                                       vk::BufferUsageFlagBits::eTransferSrc);

            memcpy(stagingBuffer.m_data, data.begin(), size);

            auto commandBuffer = beginSingleTimeCmds();

            vk::BufferCopy copyRegion{.size = size};
            commandBuffer.copyBuffer(stagingBuffer.m_buffer, buffer.m_buffer,
                                     copyRegion);

            endSingleTimeCmds(std::move(commandBuffer));

            return buffer;
        }

        ///
        /// Create a MappedBuffer that can be sequentially
        /// memcopied to from the cpu.
        ///

        MappedBuffer createMappedBuffer(vk::DeviceSize size,
                                        vk::BufferUsageFlags bufferUsage);

        Image createTexture(std::string_view path);

    private:
        SDL_Window* m_window;

        vk::DebugUtilsMessengerCreateInfoEXT m_debugInfo;

        vk::raii::Instance m_instance;
        vk::raii::SurfaceKHR m_surface;
        std::vector<const char*> m_deviceExtensions;
        vk::raii::PhysicalDevice m_physicalDevice;
        Utils::QueueFamilyIndices m_indices;
        vk::raii::Device m_device;

        vk::raii::CommandPool m_commandPool;

        vk::raii::Queue m_graphicsQueue;
        vk::raii::Queue m_presentQueue;

        SwapChainDetails m_swapChainDetails;
        vk::raii::SwapchainKHR m_swapChain;
        std::vector<vk::Image> m_swapChainImages;
        std::vector<vk::raii::ImageView> m_swapChainImageViews;

        VmaAllocator m_allocator;

        vk::raii::Instance createInstance(const vk::raii::Context& ctx,
                                          vk::ApplicationInfo applicationInfo);

        vk::raii::SurfaceKHR createSurface();
        vk::raii::PhysicalDevice pickPhysicalDevice();
        vk::raii::Device createLogicalDevice();
        vk::raii::CommandPool createCommandPool();

        SwapChainDetails querySwapChainDetails() const;

        vk::raii::SwapchainKHR
        createSwapChain(vk::SwapchainKHR old = vk::SwapchainKHR());
        std::vector<vk::Image> getSwapChainImages();
        std::vector<vk::raii::ImageView> getSwapChainImageViews();

        ///
        /// Utility function for one time command buffers and a accompanying
        /// function endSingleTimeCmds that ends and gets rid of the command
        /// buffer.
        ///
        vk::raii::CommandBuffer beginSingleTimeCmds();
        void endSingleTimeCmds(const vk::raii::CommandBuffer&& buffer);

        void transitionImageLayout(const Image& image, vk::Format format,
                                   vk::ImageLayout oldLayout,
                                   vk::ImageLayout newLayout);
    };
} // namespace Renderer
