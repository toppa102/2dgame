#pragma once

#include "MappedBuffer.hpp"
#include "StaticBuffer.hpp"
#include <Device.hpp>
#include <Utils.hpp>

#include <memory>
#include <optional>
#include <string_view>
#include <vector>

#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vulkan/vulkan_handles.hpp>
#include <vulkan/vulkan_raii.hpp>

namespace Renderer
{

    struct Vertex
    {
        glm::vec3 pos;
        glm::vec3 color;
        glm::vec2 texCoord;

        static vk::VertexInputBindingDescription getBindingDescription()
        {
            vk::VertexInputBindingDescription bindingDescription{
                .binding = 0,
                .stride = sizeof(Vertex),
                .inputRate = vk::VertexInputRate::eVertex,
            };

            return bindingDescription;
        }

        static std::array<vk::VertexInputAttributeDescription, 3>
        getAttributeDescriptions()
        {
            std::array<vk::VertexInputAttributeDescription, 3>
                attributeDescriptions{};

            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = vk::Format::eR32G32B32Sfloat;
            attributeDescriptions[0].offset = offsetof(Vertex, pos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = vk::Format::eR32G32B32Sfloat;
            attributeDescriptions[1].offset = offsetof(Vertex, color);

            attributeDescriptions[2].binding = 0;
            attributeDescriptions[2].location = 2;
            attributeDescriptions[2].format = vk::Format::eR32G32Sfloat;
            attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

            return attributeDescriptions;
        }
    };

    struct UniformBufferObject
    {
        alignas(16) glm::mat4 model;
        alignas(16) glm::mat4 view;
        alignas(16) glm::mat4 proj;
    };

    const std::vector<Vertex> vertices = {
        {{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},

        {{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{-0.5f, 0.5f, -0.5f}, {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}
    };

    const std::vector<uint16_t> indices = {
        0, 1, 2, 2, 3, 0,
        4, 5, 6, 6, 7, 4
    };

    class SDL
    {
    public:
        SDL() { SDL_Init(SDL_INIT_VIDEO); };
        ~SDL() { SDL_Quit(); };
    };

    using SDLWindowPtr =
        std::unique_ptr<SDL_Window, decltype(SDL_DestroyWindow)*>;

    class Renderer
    {
    public:
        Renderer(std::string_view app_name);
        ~Renderer();

    private:
        vk::ApplicationInfo m_appInfo;
        vk::raii::Context m_ctx;

        SDL sdl;
        SDLWindowPtr m_window;
        Device m_device;

        vk::raii::RenderPass m_renderPass;
        vk::raii::DescriptorSetLayout m_descriptorSetLayout;
        vk::raii::PipelineLayout m_pipelineLayout;
        vk::raii::Pipeline m_graphicsPipeline;

        Image m_texture;
        vk::raii::Sampler m_textureSampler;

        StaticBuffer m_vertexBuffer;
        StaticBuffer m_indexBuffer;
        std::vector<MappedBuffer> m_uniformBuffers;

        vk::raii::DescriptorPool m_descriptorPool;
        std::vector<vk::raii::DescriptorSet> m_descriptorSets;

        std::vector<vk::raii::Framebuffer> m_frameBuffers;

        std::vector<vk::raii::CommandBuffer> m_commandBuffers;

        std::vector<vk::raii::Semaphore> m_imageAvailableSemaphores;
        std::vector<vk::raii::Semaphore> m_renderFinishedSemaphores;
        std::vector<vk::raii::Fence> m_inFlightFences;
        uint32_t m_currentFrame = 0;

        bool m_frameBufferResized = false;

        void updateUniformBuffer(uint32_t currentImage)
        {
            static auto startTime = std::chrono::high_resolution_clock::now();

            auto currentTime = std::chrono::high_resolution_clock::now();
            float time =
                std::chrono::duration<float, std::chrono::seconds::period>(
                    currentTime - startTime)
                    .count();
            UniformBufferObject ubo{};
            ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f),
                                    glm::vec3(0.0f, 0.0f, 1.0f));
            ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f),
                                   glm::vec3(0.0f, 0.0f, 0.0f),
                                   glm::vec3(0.0f, 0.0f, 1.0f));
            ubo.proj = glm::perspective(
                glm::radians(45.0f),
                (float)m_device.getSwapChainDetails().extent.width /
                    (float)m_device.getSwapChainDetails().extent.height,
                0.1f, 10.0f);
            ubo.proj[1][1] *= -1;

            memcpy(m_uniformBuffers[m_currentFrame].m_data, &ubo, sizeof(ubo));
        }

        SDLWindowPtr createWindow();
        Device createDevice() const;

        void recreateSwapChain();

        vk::raii::RenderPass createRenderPass();
        vk::raii::DescriptorSetLayout createDescriptorSetLayout();
        vk::raii::PipelineLayout createPipelineLayout();
        vk::raii::Pipeline createGraphicsPipeline();

        vk::raii::Sampler createTextureSampler();

        std::vector<MappedBuffer> createUniformBuffers();

        vk::raii::DescriptorPool createDescriptorPool();
        std::vector<vk::raii::DescriptorSet> createDescriptorSets();

        std::vector<vk::raii::Framebuffer> createFrameBuffers();
        vk::raii::CommandPool createCommandPool();
        std::vector<vk::raii::CommandBuffer> createCommandBuffers();

        std::vector<vk::raii::Semaphore> createSemaphores();
        std::vector<vk::raii::Fence> createFences();

        void recordCommandBuffer(const vk::raii::CommandBuffer& commandBuffer,
                                 uint32_t imageIndex);

        void drawFrame();

        vk::raii::ShaderModule
        createShaderModule(const std::vector<char>& code);
    };
} // namespace Renderer
