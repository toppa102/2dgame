# Vulkan renderer in C++ (WIP)

A high level renderer written in C++20 with Vulkan
as backend. Highly work in progress and 
experimental because I have
just started using Vulkan.

## Architecture

[Renderer::Device](include/Device.hpp):

A wrapper for the vulkan logical device, swapchain and related dependencies.
Also handles memory allocations with the
createStaticBuffer and createMappedBuffer functions
using VulkanMemoryAllocator library,
[Renderer::StaticBuffer](include/StaticBuffer.hpp) 
and
[Renderer::MappedBuffer](include/MappedBuffer.hpp) 
objects.

[Renderer::Renderer](include/Renderer.hpp):

Contains a Renderer::Device and other needed logic for rendering.
Provides a high level api for rendering 3d meshes.
Only contains hardcoded renderpasses and subpasses
because I haven't implemented render graphs.